import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//Main Panel Components
import { MainPanelComponent } from './pages/main-panel/main-panel.component';
import { ProfileComponent } from './pages/main-panel/profile/profile.component';
import { InnerPage1Component } from './pages/main-panel/inner-page1/inner-page1.component';
import { InnerPage2Component } from './pages/main-panel/inner-page2/inner-page2.component';
//Components
import { LoginComponent } from './pages/login/login.component';
import { RegistryComponent } from './pages/registry/registry.component';
//List of routes in the sistem
const routes: Routes = [
  {
    path: '',
    redirectTo: "/login",
    pathMatch: 'full'
  },  
  { path:'login', component: LoginComponent },
  { path:'registry', component: RegistryComponent },
  { path:'main-panel', component: MainPanelComponent, //Main-panel has children routes (Visited from his parent)
    children: [
      { path:'profile', component: ProfileComponent },
      { path:'inner-page1', component: InnerPage1Component },
      { path:'inner-page2', component: InnerPage2Component },
    ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
