import { Component, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SidenavService } from '../../services/sidenav.service';
import { TokenService } from '../../services/token.service';
import { MatSnackBar } from '@angular/material';
import { MatSidenav } from '@angular/material';

@Component({
  selector: 'app-main-panel',
  templateUrl: './main-panel.component.html',
  styleUrls: ['./main-panel.component.css']
})
export class MainPanelComponent implements OnInit {

  @ViewChild('sidenav') public sidenav: MatSidenav;

  opened = true;

  action = 'Aceptar';

  constructor(
    private sidenavService: SidenavService, 
    private Router: Router,
    private Token: TokenService,
    private snackBar: MatSnackBar,
  ) { }

  links1 = [
    {url:'/main-panel/profile',showName:'Perfil de Usuario',iconSuffix:'account_circle'},
  ];

  links2 = [
    {url:'/main-panel/inner-page1',showName:'Página interna 1',iconSuffix:'assignment'},
    {url:'/main-panel/inner-page2',showName:'Página interna 2',iconSuffix:'history'},
  ]; 

  ngOnInit(){
    this.sidenavService.setSidenav(this.sidenav);
    if(this.Token.isValid()){
      setTimeout(() => {
        let message =  'Usuario autenticado correctamente';
        this.snackBar.open(message,this.action,{
          duration: 3000
        });
      },1);
    }
    else{
      this.Router.navigateByUrl('/login');
    }
    //If reload page and sidenav is opened
    this.sidenavService.open();       
  }

  logOut(){
    this.Token.remove();
    this.Router.navigateByUrl('/login');
  }
   

}
