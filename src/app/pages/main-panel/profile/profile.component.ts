import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormBuilder, FormGroup} from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Router} from '@angular/router';
import { TokenService } from '../../../services/token.service';
import { SidenavService } from '../../../services/sidenav.service';
import { MatSnackBar } from '@angular/material';
import { ErrorStateMatcher } from '@angular/material/core';
//Environment variables
import { environment } from '../../../../environments/environment';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

    return (invalidCtrl || invalidParent);
  }
}

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {

  name = new FormControl('', [Validators.required]);
  email = new FormControl('', [Validators.required, Validators.email]);
  
  action = 'Aceptar';
  
  myForm: FormGroup;

  deviceInfo = null;
  usuario = null;

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private Router: Router,
    private Token: TokenService,
    private sidenav: SidenavService,
    private snackBar: MatSnackBar
  ) { 
    this.myForm = this.formBuilder.group({
      password: ['', [Validators.required]],
      confirmPassword: ['']
    }, { validator: this.checkPasswords });
  }

  ngOnInit() {
    //If reload page and sidenav is opened
    this.sidenav.close();
    //Load Usuario
    this.getUsuario();    
  
  }
  //@ngOnDestory
  ngOnDestroy() {
    this.sidenav.open();
  }
  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;
    return pass === confirmPass ? null : { notSame: true }
  }

  getErrorMessage() {
    return this.email.hasError('required') ? 'Ingresa tu correo' :
      this.email.hasError('email') ? 'No es un correo valido' :
      '';
  }
  
  getErrorName() {
    return this.name.hasError('required') ? 'Ingresa tu nombre' : false;
  }

  update(){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });  
    let datos_usuario = {
      name : this.name.value,
      email : this.email.value,
      password: this.myForm.controls['password'].value
    }
    console.log(datos_usuario);
    this.Router.navigateByUrl('/main-panel');
    this.sidenav.toggle();
    // this.http.post('http://localhost:8000/api/register', datos_usuario , {headers}
    // ).subscribe(data => {
    //   this.succes = data['success'];
    //   if(this.succes === true)
    //     this.Router.navigateByUrl('/login');
    // },
    // error => {
    //   // console.log(error);
    //   this.errors = error;
    //   if(this.errors.status==200){
    //     let errorMessage =  'Error al registrar el usuario';
    //     this.snackBar.open(errorMessage,this.action,{
    //       duration: 3000
    //     });
    //   }
    // });    
  }

  cancel(){
    this.sidenav.toggle();
    this.Router.navigateByUrl('/main-panel');
  }

  /********************/
  /* Loading Usuario */
  /*******************/
  getUsuario(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : 'Bearer ' + this.Token.get()
    });    
    let request = {
      token: this.Token.get()
    }
    this.http.get(environment.apiUrl+'api/user', {headers:headers,params:request}
    ).subscribe(response => {
      console.log(response);
      this.usuario = response['user'];
      this.name.setValue(this.usuario.name);
      this.email.setValue(this.usuario.email);
      this.myForm.controls['password'].setValue('XXXXXX');
      this.myForm.controls['confirmPassword'].setValue('XXXXXX');
    },
    error => {
      if(error.status==401){
        if(error.error.message === 'Token has expired'){
          let errorMessage =  'La sesión ha expirado, porfavor ingrese nuevamente al sistema';
          this.snackBar.open(errorMessage,this.action,{
            duration: 3000
          });
          this.Router.navigateByUrl('/login');
        }
        else{
          let errorMessage =  'Error al adquirir el usuario desde el servidor';
          this.snackBar.open(errorMessage,this.action,{
            duration: 3000
          });
        }
      }
    });
  }  
}
