import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InnerPage2Component } from './inner-page2.component';

describe('InnerPage2Component', () => {
  let component: InnerPage2Component;
  let fixture: ComponentFixture<InnerPage2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InnerPage2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InnerPage2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
