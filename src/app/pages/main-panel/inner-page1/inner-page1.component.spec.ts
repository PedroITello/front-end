import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InnerPage1Component } from './inner-page1.component';

describe('InnerPage1Component', () => {
  let component: InnerPage1Component;
  let fixture: ComponentFixture<InnerPage1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InnerPage1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InnerPage1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
