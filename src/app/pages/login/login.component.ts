import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { TokenService } from '../../services/token.service';
import { MatSnackBar } from '@angular/material';

import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-login-form',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  message = 'Es necesario ingresar correo y contraseña';
  action = 'Aceptar';

  email = new FormControl('', [Validators.email]); //Validators.required,
  
  errors = null;
  password =  null;
  token = null;

  getErrorMessage() {
    // this.email.hasError('required') ? 'Ingresa tu correo' :
    return this.email.hasError('email') ? 'No es un correo valido' :
            '';
  } 

  constructor(
    private http: HttpClient,
    private Router: Router,
    private Token: TokenService,
    private snackBar: MatSnackBar
    ) { }

  ngOnInit() {
    if(!this.Token.isValid() && sessionStorage.getItem('login') === 'true'){
      setTimeout(() => {
        this.snackBar.open(this.message,this.action,{
          duration: 3000
        });
      },1);
    }
  }

  login(){
    sessionStorage.setItem('login', 'true');
    // console.log(this.email.value+'<>'+this.password)
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });  
    let request = {
      email : this.email.value,
      password: this.password
    }
    this.http.post(environment.apiUrl+'api/login', request , {headers}
    ).subscribe(response => {
      // console.log(response);
      this.token = response['token'];
      this.Token.handle(this.token);
      this.Router.navigateByUrl('/main-panel');
    },
    error => {
      this.errors = error;
      if(this.errors.status==401){
        let errorMessage =  'correo o contraseña incorrectos';
        this.snackBar.open(errorMessage,this.action,{
          duration: 3000
        });
      }
      else if(this.errors.status==500){
        let errorMessage =  'Error al conectar a la base de datos del servidor';
        this.snackBar.open(errorMessage,this.action,{
          duration: 3000
        });
      }
    });
    // console.log("Login");
  }

  onKeydown(event) {
    if (event.key === "Enter") {
      this.login();
    }
  }
}
