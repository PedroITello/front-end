import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormBuilder, FormGroup} from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Router} from '@angular/router';
import { TokenService } from '../../services/token.service';
import { MatSnackBar } from '@angular/material';
import { ErrorStateMatcher } from '@angular/material/core';

import { environment } from '../../../environments/environment';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

    return (invalidCtrl || invalidParent);
  }
}

@Component({
  selector: 'app-registry',
  templateUrl: './registry.component.html',
  styleUrls: ['./registry.component.css']
})
export class RegistryComponent implements OnInit {
  name = new FormControl('', [Validators.required]);
  email = new FormControl('', [Validators.required, Validators.email]);
  errors = null;
  password =  null;
  succes = null;

  action = 'Aceptar';
  
  myForm: FormGroup;

  matcher = new MyErrorStateMatcher();

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private Router: Router,
    private Token: TokenService,
    private snackBar: MatSnackBar
  ){ 
    this.myForm = this.formBuilder.group({
      password: ['', [Validators.required,Validators.pattern('[A-Za-z0-9/-]{6,}')]],
      confirmPassword: ['', [Validators.pattern('[A-Za-z0-9/-]{6,}')]]
    }, { validator: this.checkPasswords });
  }

  ngOnInit() {
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : { notSame: true }
  }

  getErrorMessage() {
    return this.email.hasError('required') ? 'Ingresa tu correo' :
        this.email.hasError('email') ? 'No es un correo valido' :
            '';
  }
  
  getErrorName() {
    return this.name.hasError('required') ? 'Ingresa tu nombre' : false;
  }

  registry(){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });  
    let request = {
      name : this.name.value,
      email : this.email.value,
      password: this.myForm.controls['password'].value
    }
    this.http.post(environment.apiUrl+'api/register', request , {headers}
    ).subscribe(response => {
      this.succes = response['success'];
      if(this.succes === true)
        this.Router.navigateByUrl('/login');
    },
    error => {
      // console.log(error);
      this.errors = error;
      if(this.errors.status==200){
        let errorMessage =  'Error al registrar el usuario';
        this.snackBar.open(errorMessage,this.action,{
          duration: 3000
        });
      }
    });    
  }
}
