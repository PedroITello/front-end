import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { SidenavService } from '../../services/sidenav.service';
import { TokenService } from '../../services/token.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isLoggedIn$: Observable<boolean>;   

  constructor(
    private sidenav: SidenavService,
    private Token: TokenService
    ) { }   

  toggleActive:boolean = true;

  ngOnInit() {
    this.isLoggedIn$ = this.Token.isLoggedIn;
  }

	toggleRightSidenav() {
		this.toggleActive = !this.toggleActive;
		this.sidenav.toggle();
  }
}
