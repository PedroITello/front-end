/*******************/
/* Sidenav Service */
/*******************/
import { Injectable } from '@angular/core';
import { MatSidenav } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class SidenavService {
  private sidenav: MatSidenav;

  constructor() { }

  /**
  * This is the setSidenav function
  * @param sidenav This is the MatSidenav component
  * 
  */
  public setSidenav(sidenav: MatSidenav) {
    this.sidenav = sidenav;
  }

  /**
  * This is the open function
  * @returns returns execution of open action in sidenav
  * 
  */  
  public open() {
    return this.sidenav.open();
  }

  /**
  * This is the close functin
  * @returns returns execution of close action in sidenav
  * 
  */ 
  public close() {
    return this.sidenav.close();
  }

  /**
  * This is the toggle functin
  * Execute the sidenav function of the MatSidenav component
  * 
  */ 
  public toggle(): void {
    this.sidenav.toggle();
  }  
}
