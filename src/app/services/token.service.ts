import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
//Environment variables
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  get isLoggedIn(){
    // console.log(sessionStorage.getItem('logged'));
    if(sessionStorage.getItem('logged')==='true')
      this.loggedIn.next(true);
    return this.loggedIn.asObservable();
  }  

  constructor() { }

  handle(token){
    this.set(token);
  }

  set(token){
    sessionStorage.setItem('token', token);
    sessionStorage.setItem('logged', 'true');
    this.loggedIn.next(true);   
  }

  get(){
    return sessionStorage.getItem('token');
  }

  remove(){
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('login');
    sessionStorage.removeItem('logged');
    this.loggedIn.next(false);
  }

  isValid(){
    const token = this.get();
    if(token){
      const payload = this.payload(token);
      if(payload){
        return (payload.iss = environment.apiUrl+'api/login')?true:false;
      }
    }
    return false;
  }

  payload(token){
    const payload = token.split('.')[1];
    return this.decode(payload);
  }

  decode(payload){
    return JSON.parse(atob(payload));
  }
}
