import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// Modules
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxPrintModule } from 'ngx-print';
//External material modules
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
//Core Components
import { AppComponent } from './app.component';
import { HeaderComponent } from './core/header/header.component';
import { FooterComponent } from './core/footer/footer.component';
//Main Panel Components
import { MainPanelComponent } from './pages/main-panel/main-panel.component';
import { ProfileComponent } from './pages/main-panel/profile/profile.component';
import { InnerPage1Component } from './pages/main-panel/inner-page1/inner-page1.component';
import { InnerPage2Component } from './pages/main-panel/inner-page2/inner-page2.component';
//Components
import { LoginComponent } from './pages/login/login.component';
import { RegistryComponent } from './pages/registry/registry.component';
//Services
import { SidenavService } from './services/sidenav.service';
import { TokenService } from './services/token.service';
//Librarys
import 'hammerjs';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MainPanelComponent,
    LoginComponent,
    RegistryComponent,
    ProfileComponent,
    InnerPage1Component,
    InnerPage2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule, 
    ReactiveFormsModule,
    HttpClientModule,
    NgxPrintModule,
    MaterialFileInputModule,
    NgxMatSelectSearchModule
  ],
  providers: [
    SidenavService,
    TokenService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
