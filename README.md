# Capa de Aplicación

Este proyecto es generado utilizndo [Angular CLI](https://github.com/angular/angular-cli) version 7.1.4.

## Objetivo del proyecto

Proveer un marco de trabajo que considere las mejores prácticas para el desarrollo de aplicaciones web seguras.

## Contenido

En este apartado se describe de forma general el contenido del proyecto.

### Prerequisitos

**Instalar:**

*Dependencias:*

* [GIT](https://git-scm.com/)   -   Contról de vesionamiento.
* [Node.js](https://nodejs.org/es/) -   Entorno de ejecución JS (NPM).
* [Visual Code](https://code.visualstudio.com/) - IDE de desarrollo recomendada.

*Angular:*

* [AngularCLI](https://cli.angular.io/) -   Interface de línea de comandos para Angular. 

### Librerías adiconadas al proyecto

* [Angular Material](https://material.angular.io/) -   Diseño Material para Angular.
* [Hammer JS](https://hammerjs.github.io/)  -   Librería complementaria a Angular-Material.
* [Flex-Layout](https://github.com/angular/flex-layout) - Librería para el maquetamiento de la IU.
* [Moment](https://momentjs.com/) - Librería para el tratamiento de fechas en JS.
* [Material-moment-adapter](https://www.npmjs.com/package/@angular/material-moment-adapter) -   Librería complementaria para el manejo de moment en el diseño Material.
* [ngx-material-file-input](https://github.com/merlosy/ngx-material-file-input) -   Campo de entrada para archivos estilo Material.
* [ngx-mat-select-search](https://www.npmjs.com/package/ngx-mat-select-search) - Campo para búsqueda en selectores.
* [ngx-print](https://www.npmjs.com/package/ngx-print) - Directiva para imprimir las secciones HTML con Angular.


### Módulos integrados al proyecto

* *Servicios:*
    * Servicio para el manejo de JWT
    * Servicio para el manejo de la barra lateral

* *Páginas:*
    *   Estructura principal
    *   Página de inicio de sesión.
    *   Página de registro de usuario.
    *   Página principal de ejemplo.

## Instalación.

* Descargar la rama de desarrollo-2019 (Temporal hasta el primer Merge del proyecto a Master) mediante el uso de GIT. El comando para realizar esta acción es :\> git clone --branch desarrollo-2019 https://bitbucket.org/PedroITello/front-end.git
* En el folder del proyecto ejecutar el comando :\> npm install y esperar a que finalice la instalación de las dependencias.
* Ejecutar :\>npm audit fix para arreglar problemas entre dependencias.
* En caso de necesitarse ejecutar npm update para actualizar las dependencias. *Nota:* En caso de ser necesario se tendrá que actualizar dependencia por dependencia cuando no se pueda ejecutar de forma masiva. Ejemplo: npm update @angular/core.
* Ejecutar el comando :\> ng serve y visitar en el navegador la URL <http://localhost:4200> para comprobar el funcionamiento del marco de trabajo.
